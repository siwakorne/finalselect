import React, { Component } from 'react';
// import logo from './logo.svg';
import './App.css';

class App extends Component {
  constructor (props) {
    super(props)
    this.state = {
      rent: '',
      interest: '',
      month: '',
      age: '',
      startMonth: '',
      dayperMonth: ['30', '30', '30', '30', '30', '30', '30', '30', '30', '30', '30', '30'],
      table: '',
      monthMoney: '',
      click: false
    }
    this.Calculator = this.Calculator.bind(this)
  }
  
  handleChange1 (e) {
    this.setState({rent: Number(e.target.value)});
  }

  handleChange2 (e) {
    this.setState({interest: e.target.value});
  }

  handleChange3 (e) {
    this.setState({month: Number(e.target.value)});
  }

  handleChange4 (e) {
    this.setState({startMonth: e.target.value})
  }

  handleChange5 (e) {
    this.setState({age: Number(e.target.value)})
  }

  handleChange6 (e) {
    this.setState({monthMoney: e.target.value})
  }
  Calculator () {
    if ((Number(this.state.age) + Number(this.state.month / 12) >= 61)) {
      alert('อายุเกินเกณฑ์ผ่อนชำระ (อายุ+ระยะผ่อน ไม่ควรเกิน60ปี)')
    }
    else {
      if ((this.state.age && this.state.month) || (this.state.monthMoney && this.state.age) || (this.state.month && this.state.monthMoney)) {
        alert('กรุณาเลือกกรอกระหว่าง ผ่อนชำระ, อายุ หรือ จำนวนเงินแต่ละงวด เพียงช่องเดียว!')
      } else {
        if (this.state.rent && this.state.interest && this.state.startMonth && (this.state.month || this.state.age || this.state.monthMoney)) {
          let rentLoop = this.state.rent
          let tableCal = [<tr><td>งวดที่</td><td>เงินกู้/เงินกู้เหลือ</td><td>จำนวนวัน</td><td>เงินต้น</td><td>ดอกเบี้ย</td><td>ยอดชำระต่องวด</td></tr>]
          let inter = 0
          let interCal = 0
          let Money = 0
          let sum = 0
          let sumPerMonth = 0
          let beginMoney = 0
          let startMonthLoop = 0
          let overLoop = 0
          let a = this.state.startMonth
          if (this.state.monthMoney) {
            startMonthLoop = Math.ceil(this.state.rent / this.state.monthMoney)
            console.log(startMonthLoop)
          }
          else if (this.state.age) {
            startMonthLoop = (60-this.state.age)*12
          } else {
            startMonthLoop = this.state.month
            interCal = parseFloat(this.state.interest/100)/12
            let tailMath = ((1-(1/Math.pow((1+interCal),this.state.month)))/interCal)
            sumPerMonth = (this.state.rent/tailMath).toFixed(2)
          }
          for(let i=0; i<(startMonthLoop + overLoop); i++) {
            let loopMonth = a
            if (loopMonth >= 12) loopMonth = (loopMonth % 12)
            inter = rentLoop * ((this.state.interest/100 / 365 * this.state.dayperMonth[loopMonth]))
            inter = inter.toFixed(2)
            if(!this.state.monthMoney) {
              // beginMoney = Math.ceil(this.state.rent / startMonthLoop) / 10
              // beginMoney = Math.ceil(beginMoney)*10
              beginMoney = (sumPerMonth-inter).toFixed(2)
            }
            Money = Money + beginMoney
            if (Money > this.state.rent) {
              beginMoney = beginMoney -(Money - this.state.rent)
            }
            if (!this.state.monthMoney) {
              sumPerMonth = (Number(inter) + Number(beginMoney)).toFixed(2)
            } else {
              sumPerMonth = this.state.monthMoney
              beginMoney = (sumPerMonth - inter).toFixed(2)
            }
            if (startMonthLoop <= i+1 && this.state.monthMoney) {
              var total = Math.ceil(rentLoop-sumPerMonth)
              if (Number(total) < 0){
                beginMoney = rentLoop
                sumPerMonth = (Number(beginMoney) + Number(inter)).toFixed(2)
              }
            }
            if(!this.state.monthMoney) {
              if(rentLoop-beginMoney < 0) {
                beginMoney = rentLoop.toFixed(2)
                rentLoop = 0
                sumPerMonth = (parseFloat(beginMoney)+parseFloat(inter)).toFixed(2)
              } else {
                rentLoop = (rentLoop-beginMoney)
              }
            } else {
              rentLoop = (rentLoop-beginMoney).toFixed(2)
              sum += Number(beginMoney)
              if ((startMonthLoop-1) === i) {
                overLoop = Math.ceil(rentLoop/this.state.monthMoney)
                if(rentLoop-beginMoney > 0) {
                  overLoop++
                }
              }
            }
            if (beginMoney > 0) {
              tableCal.push(
              <tr>
                <td>
                  {i+1}
                </td>
                <td>
                  {rentLoop > 0 ? rentLoop : '-'}
                </td>
                <td>
                  {this.state.dayperMonth[loopMonth]}
                </td>
                <td>
                  {beginMoney}
                </td>
                <td>
                  {inter}
                </td>
                <td>
                  {sumPerMonth}
                </td>
              </tr>
              )
            }  
            a++;
          }
          this.setState({table: tableCal,click: true, total: sum})
        } else {
          alert('ข้อมูลที่คุณกรอกไม่ครบถ้วน')
        }
      }
    }
  }

  render() {
    return (
      <div className="App">
        {/* <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header> */}
        <table>
          <tr>
            <td>
              ยอดเงินกู้ 
            </td>
            <td>
                <input type="text" value={this.state.rent} onChange={this.handleChange1.bind(this)} />
            </td>
          </tr>
          <tr>
            <td>
              อัตราดอกเบี้ย (% ต่อปี)
            </td> 
            <td>
              <input type="text" value={this.state.interest} onChange={this.handleChange2.bind(this)} />
            </td>
          </tr>
          <tr>
            <td>
              *ผ่อนชำระ (งวด)
            </td>
            <td>
              <input type="text" value={this.state.month} onChange={this.handleChange3.bind(this)} />
            </td>
          </tr>
          <tr>
            <td>
              *อายุ
            </td>
            <td>
              <input type="text" value={this.state.age} onChange={this.handleChange5.bind(this)} />
            </td>
          </tr>
          <tr>
            <td>
              *จำนวนเงินแต่ละงวด ที่ต้องการผ่อน
            </td>
            <td>
              <input type="text" value={this.state.monthMoney} onChange={this.handleChange6.bind(this)} />
            </td>
          </tr>
          <tr>
            <td>
              เดือนที่เริ่มชำระ
            </td>
            <td>
              <select value={this.state.startMonth} onChange={this.handleChange4.bind(this)}>
                <option value="">เลือกเดือน</option>                 
                <option value="0">มกราคม</option>
                <option value="1">กุมภาพันธ์</option>
                <option value="2">มีนาคม</option>
                <option value="3">เมษายน</option>
                <option value="4">พฤษภาคม</option>
                <option value="5">มิถุนายน</option>
                <option value="6">กรกฎาคม</option>
                <option value="7">สิงหาคม</option>
                <option value="8">กันยายน</option>
                <option value="9">ตุลาคม</option>
                <option value="10">พฤศจิกายน</option>
                <option value="11">ธันวาคม</option>
              </select>
            </td>
          </tr>
          <tr>
            <td>
            </td>
            <td>
              *กรุณาเลือกกรอกระหว่าง ผ่อนชำระงวด, อายุ หรือ จำนวนเงินแต่ละงวด
            </td>
          </tr>
          <tr>
            <td>
            </td>
            <td>
              <button onClick={this.Calculator}>คำนวณ</button>
            </td>
          </tr>
        </table>
        <table>
          {this.state.table}
        </table>
      </div>
    );
  }
}

export default App;
